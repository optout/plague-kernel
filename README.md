### What is the Plague kernel?
The Plague kernel was initially designed for PlagueOS (hardened Void Linux musl), however the project has taken a life of its own. The kernel is distro-agnostic, therefore it can be ran on Debian, Fedora, Void Linux, and many more. The script detailed below allows for self-compilation of the Plague kernel, along with the option to tailor a custom kernel to your specific hardware.

- [host_hardened.config](https://0xacab.org/optout/plague-kernel/-/blob/main/host_hardened.config?ref_type=heads) - Feature-complete baseline designed for the host
- [virt_hardened.config](https://0xacab.org/optout/plague-kernel/-/blob/main/virt_hardened.config?ref_type=heads) - Work-in-progress baseline designed to work within various guest Virtual Machines
    - Note: This is currently bootable, but a comprehensive test is still needed within a set of VMs. Most notable issues at the moment are video/media drivers preventing video playback

The script performs a variety of functions:
- Installs needed dependencies for compilation
- Stages desired linux-hardened kernel under `/usr/src/`
- Imports Plague kernel configuration (KSPP recommendations applied + trimming)
- (Optional) Cut attack surface further by evaluating mapped modules on current system, thus deselecting a variety of bloat from the configuration
- Remove unnecessary artifacts such as System.map & signing keys

### Steps to self-compile
1. Run `bash self_compilation.sh`
2. Set the Kernel version (KVER) variable to a released version you want to obtain from Anthraxx's [linux-hardened](https://github.com/anthraxx/linux-hardened/releases) repository

> Note: If you are planning on mapping modules used by your system when prompted via the script, we highly recommend that you access peripherals such as Webcams, adapters, USBs, protocols such as Bluetooth prior to running the script, if they are desired. 


### Trimming Efforts
While linux-hardened security patchsets along with kernel configurations are notable for this kernel project, the purpose was to practice minimalism by reducing the size of the linux kernel, thereby cutting attack surface. This is not a trivial thing to record, therefore we are displaying the size purely as a point of comparison.

| | Plague (Virt) | Plague (Host) | TAILS | Whonix | Vanilla |
| --- | --- |--- | --- | --- | --- | 
| Size (/lib/modules/)| 1.8 MB | 31.0 MB | 89.0 MB | 89.0 MB | 126.0 MB |
| Size (vmlinuz) | 7.9 MB | 8.0 MB | 7.8 MB | 7.8 MB | 14.0 MB | 
| No. of modules | 69 | 1409 | 4039 | 4044 | 4402 |

#### Additional Resources:
- https://docs.clip-os.org/clipos/kernel.html
- https://github.com/anthraxx/linux-hardened
- https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project
- https://madaidans-insecurities.github.io/guides/linux-hardening.html#kernel

### Current [kernel-hardening-checker](https://github.com/a13xp0p0v/kernel-hardening-checker) results
#### Successes

Option | Desired Value | Source | Reason | Result |
|--- | --- | --- | --- | --- |
CONFIG_BUG                              |     y      |defconfig | self_protection  | OK
CONFIG_THREAD_INFO_IN_TASK              |     y      |defconfig | self_protection  | OK
CONFIG_IOMMU_SUPPORT                    |     y      |defconfig | self_protection  | OK
CONFIG_STACKPROTECTOR                   |     y      |defconfig | self_protection  | OK
CONFIG_STACKPROTECTOR_STRONG            |     y      |defconfig | self_protection  | OK
CONFIG_STRICT_KERNEL_RWX                |     y      |defconfig | self_protection  | OK
CONFIG_STRICT_MODULE_RWX                |     y      |defconfig | self_protection  | OK
CONFIG_REFCOUNT_FULL                    |     y      |defconfig | self_protection  | OK: version >= 5.5
CONFIG_INIT_STACK_ALL_ZERO              |     y      |defconfig | self_protection  | OK
CONFIG_RANDOMIZE_BASE                   |     y      |defconfig | self_protection  | OK
CONFIG_VMAP_STACK                       |     y      |defconfig | self_protection  | OK
CONFIG_SPECULATION_MITIGATIONS          |     y      |defconfig | self_protection  | OK
CONFIG_DEBUG_WX                         |     y      |defconfig | self_protection  | OK
CONFIG_WERROR                           |     y      |defconfig | self_protection  | OK
CONFIG_X86_MCE                          |     y      |defconfig | self_protection  | OK
CONFIG_X86_MCE_INTEL                    |     y      |defconfig | self_protection  | OK
CONFIG_X86_MCE_AMD                      |     y      |defconfig | self_protection  | OK
CONFIG_RETPOLINE                        |     y      |defconfig | self_protection  | OK
CONFIG_SYN_COOKIES                      |     y      |defconfig | self_protection  | OK
CONFIG_MICROCODE                        |     y      |defconfig | self_protection  | OK
CONFIG_MICROCODE_INTEL                  |     y      |defconfig | self_protection  | OK: CONFIG_MICROCODE is "y"
CONFIG_MICROCODE_AMD                    |     y      |defconfig | self_protection  | OK: CONFIG_MICROCODE is "y"
CONFIG_X86_SMAP                         |     y      |defconfig | self_protection  | OK: version >= 5.19
CONFIG_X86_UMIP                         |     y      |defconfig | self_protection  | OK
CONFIG_PAGE_TABLE_ISOLATION             |     y      |defconfig | self_protection  | OK
CONFIG_RANDOMIZE_MEMORY                 |     y      |defconfig | self_protection  | OK
CONFIG_X86_KERNEL_IBT                   |     y      |defconfig | self_protection  | OK
CONFIG_CPU_SRSO                         |     y      |defconfig | self_protection  | OK
CONFIG_INTEL_IOMMU                      |     y      |defconfig | self_protection  | OK
CONFIG_AMD_IOMMU                        |     y      |defconfig | self_protection  | OK
CONFIG_BUG_ON_DATA_CORRUPTION           |     y      |   kspp   | self_protection  | OK
CONFIG_SLAB_FREELIST_HARDENED           |     y      |   kspp   | self_protection  | OK
CONFIG_SLAB_FREELIST_RANDOM             |     y      |   kspp   | self_protection  | OK
CONFIG_SHUFFLE_PAGE_ALLOCATOR           |     y      |   kspp   | self_protection  | OK
CONFIG_FORTIFY_SOURCE                   |     y      |   kspp   | self_protection  | OK
CONFIG_DEBUG_LIST                       |     y      |   kspp   | self_protection  | OK
CONFIG_INIT_ON_ALLOC_DEFAULT_ON         |     y      |   kspp   | self_protection  | OK
CONFIG_SCHED_CORE                       |     y      |   kspp   | self_protection  | OK
CONFIG_SCHED_STACK_END_CHECK            |     y      |   kspp   | self_protection  | OK
CONFIG_KFENCE                           |     y      |   kspp   | self_protection  | OK
CONFIG_KFENCE_SAMPLE_INTERVAL           | is not off |    my    | self_protection  | OK: is not off, "100"
CONFIG_HARDENED_USERCOPY                |     y      |   kspp   | self_protection  | OK
CONFIG_HARDENED_USERCOPY_FALLBACK       | is not set |   kspp   | self_protection  | OK: is not found
CONFIG_HARDENED_USERCOPY_PAGESPAN       | is not set |   kspp   | self_protection  | OK: is not found
CONFIG_MODULE_SIG                       |     y      |   kspp   | self_protection  | OK
CONFIG_MODULE_SIG_ALL                   |     y      |   kspp   | self_protection  | OK
CONFIG_MODULE_SIG_SHA512                |     y      |   kspp   | self_protection  | OK
CONFIG_MODULE_SIG_FORCE                 |     y      |   kspp   | self_protection  | OK
CONFIG_INIT_ON_FREE_DEFAULT_ON          |     y      |   kspp   | self_protection  | OK
CONFIG_EFI_DISABLE_PCI_DMA              |     y      |   kspp   | self_protection  | OK
CONFIG_RESET_ATTACK_MITIGATION          |     y      |   kspp   | self_protection  | OK
CONFIG_RANDOMIZE_KSTACK_OFFSET_DEFAULT  |     y      |   kspp   | self_protection  | OK
CONFIG_HW_RANDOM_TPM                    |     y      |   kspp   | self_protection  | OK
CONFIG_DEFAULT_MMAP_MIN_ADDR            |   65536    |   kspp   | self_protection  | OK
CONFIG_IOMMU_DEFAULT_DMA_STRICT         |     y      |   kspp   | self_protection  | OK
CONFIG_IOMMU_DEFAULT_PASSTHROUGH        | is not set |   kspp   | self_protection  | OK
CONFIG_INTEL_IOMMU_DEFAULT_ON           |     y      |   kspp   | self_protection  | OK
CONFIG_SLS                              |     y      |   kspp   | self_protection  | OK
CONFIG_INTEL_IOMMU_SVM                  |     y      |   kspp   | self_protection  | OK
CONFIG_AMD_IOMMU_V2                     |     y      |   kspp   | self_protection  | OK
CONFIG_SLAB_MERGE_DEFAULT               | is not set |  clipos  | self_protection  | OK
CONFIG_LIST_HARDENED                    |     y      |    my    | self_protection  | OK
CONFIG_RANDOM_KMALLOC_CACHES            |     y      |    my    | self_protection  | OK
CONFIG_SECURITY                         |     y      |defconfig | security_policy  | OK
CONFIG_SECURITY_YAMA                    |     y      |   kspp   | security_policy  | OK
CONFIG_SECURITY_LANDLOCK                |     y      |   kspp   | security_policy  | OK
CONFIG_SECURITY_SELINUX_DISABLE         | is not set |   kspp   | security_policy  | OK: is not found
CONFIG_SECURITY_LOCKDOWN_LSM            |     y      |   kspp   | security_policy  | OK
CONFIG_SECURITY_LOCKDOWN_LSM_EARLY      |     y      |   kspp   | security_policy  | OK
CONFIG_LOCK_DOWN_KERNEL_FORCE_CONFIDENTIALITY|     y      |   kspp   | security_policy  | OK
CONFIG_SECURITY_WRITABLE_HOOKS          | is not set |   kspp   | security_policy  | OK: is not found
CONFIG_SECURITY_SELINUX_DEBUG           | is not set |    my    | security_policy  | OK
CONFIG_SECURITY_SELINUX                 |     y      |    my    | security_policy  | OK
CONFIG_SECCOMP                          |     y      |defconfig |cut_attack_surface| OK
CONFIG_SECCOMP_FILTER                   |     y      |defconfig |cut_attack_surface| OK
CONFIG_BPF_UNPRIV_DEFAULT_OFF           |     y      |defconfig |cut_attack_surface| OK
CONFIG_STRICT_DEVMEM                    |     y      |defconfig |cut_attack_surface| OK: CONFIG_DEVMEM is "is not set"
CONFIG_X86_INTEL_TSX_MODE_OFF           |     y      |defconfig |cut_attack_surface| OK
CONFIG_SECURITY_DMESG_RESTRICT          |     y      |   kspp   |cut_attack_surface| OK
CONFIG_ACPI_CUSTOM_METHOD               | is not set |   kspp   |cut_attack_surface| OK: is not found
CONFIG_COMPAT_BRK                       | is not set |   kspp   |cut_attack_surface| OK
CONFIG_DEVKMEM                          | is not set |   kspp   |cut_attack_surface| OK: is not found
CONFIG_INET_DIAG                        | is not set |   kspp   |cut_attack_surface| OK
CONFIG_KEXEC                            | is not set |   kspp   |cut_attack_surface| OK
CONFIG_PROC_KCORE                       | is not set |   kspp   |cut_attack_surface| OK
CONFIG_LEGACY_PTYS                      | is not set |   kspp   |cut_attack_surface| OK
CONFIG_HIBERNATION                      | is not set |   kspp   |cut_attack_surface| OK
CONFIG_COMPAT                           | is not set |   kspp   |cut_attack_surface| OK: is not found
CONFIG_IA32_EMULATION                   | is not set |   kspp   |cut_attack_surface| OK
CONFIG_X86_X32                          | is not set |   kspp   |cut_attack_surface| OK: is not found
CONFIG_X86_X32_ABI                      | is not set |   kspp   |cut_attack_surface| OK
CONFIG_MODIFY_LDT_SYSCALL               | is not set |   kspp   |cut_attack_surface| OK
CONFIG_OABI_COMPAT                      | is not set |   kspp   |cut_attack_surface| OK: is not found
CONFIG_X86_MSR                          | is not set |   kspp   |cut_attack_surface| OK
CONFIG_LEGACY_TIOCSTI                   | is not set |   kspp   |cut_attack_surface| OK
CONFIG_DEVMEM                           | is not set |   kspp   |cut_attack_surface| OK
CONFIG_IO_STRICT_DEVMEM                 |     y      |   kspp   |cut_attack_surface| OK: CONFIG_DEVMEM is "is not set"
CONFIG_LDISC_AUTOLOAD                   | is not set |   kspp   |cut_attack_surface| OK
CONFIG_COMPAT_VDSO                      | is not set |   kspp   |cut_attack_surface| OK: is not found
CONFIG_X86_VSYSCALL_EMULATION           | is not set |   kspp   |cut_attack_surface| OK
CONFIG_ZSMALLOC_STAT                    | is not set |  grsec   |cut_attack_surface| OK
CONFIG_PAGE_OWNER                       | is not set |  grsec   |cut_attack_surface| OK
CONFIG_DEBUG_KMEMLEAK                   | is not set |  grsec   |cut_attack_surface| OK
CONFIG_BINFMT_AOUT                      | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_KPROBE_EVENTS                    | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_UPROBE_EVENTS                    | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_GENERIC_TRACER                   | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_FUNCTION_TRACER                  | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_STACK_TRACER                     | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_HIST_TRIGGERS                    | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_BLK_DEV_IO_TRACE                 | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_PROC_VMCORE                      | is not set |  grsec   |cut_attack_surface| OK
CONFIG_PROC_PAGE_MONITOR                | is not set |  grsec   |cut_attack_surface| OK
CONFIG_USELIB                           | is not set |  grsec   |cut_attack_surface| OK
CONFIG_CHECKPOINT_RESTORE               | is not set |  grsec   |cut_attack_surface| OK
CONFIG_USERFAULTFD                      | is not set |  grsec   |cut_attack_surface| OK
CONFIG_HWPOISON_INJECT                  | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_MEM_SOFT_DIRTY                   | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_DEVPORT                          | is not set |  grsec   |cut_attack_surface| OK
CONFIG_DEBUG_FS                         | is not set |  grsec   |cut_attack_surface| OK
CONFIG_NOTIFIER_ERROR_INJECTION         | is not set |  grsec   |cut_attack_surface| OK
CONFIG_FAIL_FUTEX                       | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_PUNIT_ATOM_DEBUG                 | is not set |  grsec   |cut_attack_surface| OK
CONFIG_ACPI_CONFIGFS                    | is not set |  grsec   |cut_attack_surface| OK
CONFIG_EDAC_DEBUG                       | is not set |  grsec   |cut_attack_surface| OK
CONFIG_DRM_I915_DEBUG                   | is not set |  grsec   |cut_attack_surface| OK
CONFIG_BCACHE_CLOSURES_DEBUG            | is not set |  grsec   |cut_attack_surface| OK
CONFIG_DVB_C8SECTPFE                    | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_MTD_SLRAM                        | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_MTD_PHRAM                        | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_IO_URING                         | is not set |  grsec   |cut_attack_surface| OK
CONFIG_RSEQ                             | is not set |  grsec   |cut_attack_surface| OK
CONFIG_LATENCYTOP                       | is not set |  grsec   |cut_attack_surface| OK
CONFIG_KCOV                             | is not set |  grsec   |cut_attack_surface| OK
CONFIG_PROVIDE_OHCI1394_DMA_INIT        | is not set |  grsec   |cut_attack_surface| OK
CONFIG_SUNRPC_DEBUG                     | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_PTDUMP_DEBUGFS                   | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_DRM_LEGACY                       | is not set |maintainer|cut_attack_surface| OK
CONFIG_BLK_DEV_FD                       | is not set |maintainer|cut_attack_surface| OK: is not found
CONFIG_BLK_DEV_FD_RAWCMD                | is not set |maintainer|cut_attack_surface| OK: is not found
CONFIG_NOUVEAU_LEGACY_CTX_SUPPORT       | is not set |maintainer|cut_attack_surface| OK: is not found
CONFIG_STAGING                          | is not set |  clipos  |cut_attack_surface| OK
CONFIG_KSM                              | is not set |  clipos  |cut_attack_surface| OK
CONFIG_KALLSYMS                         | is not set |  clipos  |cut_attack_surface| OK
CONFIG_MAGIC_SYSRQ                      | is not set |  clipos  |cut_attack_surface| OK
CONFIG_KEXEC_FILE                       | is not set |  clipos  |cut_attack_surface| OK
CONFIG_X86_CPUID                        | is not set |  clipos  |cut_attack_surface| OK
CONFIG_X86_IOPL_IOPERM                  | is not set |  clipos  |cut_attack_surface| OK
CONFIG_ACPI_TABLE_UPGRADE               | is not set |  clipos  |cut_attack_surface| OK
CONFIG_EFI_CUSTOM_SSDT_OVERLAYS         | is not set |  clipos  |cut_attack_surface| OK
CONFIG_AIO                              | is not set |  clipos  |cut_attack_surface| OK
CONFIG_EFI_TEST                         | is not set | lockdown |cut_attack_surface| OK
CONFIG_MMIOTRACE_TEST                   | is not set | lockdown |cut_attack_surface| OK: is not found
CONFIG_KPROBES                          | is not set | lockdown |cut_attack_surface| OK
CONFIG_MMIOTRACE                        | is not set |    my    |cut_attack_surface| OK: is not found
CONFIG_LIVEPATCH                        | is not set |    my    |cut_attack_surface| OK: is not found
CONFIG_IP_DCCP                          | is not set |    my    |cut_attack_surface| OK
CONFIG_IP_SCTP                          | is not set |    my    |cut_attack_surface| OK
CONFIG_FTRACE                           | is not set |    my    |cut_attack_surface| OK
CONFIG_VIDEO_VIVID                      | is not set |    my    |cut_attack_surface| OK
CONFIG_INPUT_EVBUG                      | is not set |    my    |cut_attack_surface| OK
CONFIG_KGDB                             | is not set |    my    |cut_attack_surface| OK
CONFIG_CORESIGHT                        | is not set |    my    |cut_attack_surface| OK: is not found
CONFIG_XFS_SUPPORT_V4                   | is not set |    my    |cut_attack_surface| OK: is not found
CONFIG_TRIM_UNUSED_KSYMS                |     y      |    my    |cut_attack_surface| OK
CONFIG_MODULE_FORCE_LOAD                | is not set |    my    |cut_attack_surface| OK
CONFIG_COREDUMP                         | is not set |  clipos  | harden_userspace | OK
CONFIG_ARCH_MMAP_RND_BITS               |     32     |    my    | harden_userspace | OK
CONFIG_BINFMT_MISC                      | is not set |   kspp   |cut_attack_surface| OK

#### Fails
Option | Desired Value | Source | Reason | Result |
|--- | --- | --- | --- | --- |
CONFIG_SLUB_DEBUG                       |     y      |defconfig | self_protection  | FAIL: "is not set"
CONFIG_GCC_PLUGINS                      |     y      |defconfig | self_protection  | FAIL: is not found
CONFIG_DEBUG_VIRTUAL                    |     y      |   kspp   | self_protection  | FAIL: "is not set"
CONFIG_DEBUG_SG                         |     y      |   kspp   | self_protection  | FAIL: "is not set"
CONFIG_DEBUG_CREDENTIALS                |     y      |   kspp   | self_protection  | FAIL: is not found
CONFIG_STATIC_USERMODEHELPER            |     y      |   kspp   | self_protection  | FAIL: "is not set"
CONFIG_DEBUG_NOTIFIERS                  |     y      |   kspp   | self_protection  | FAIL: "is not set"
CONFIG_RANDSTRUCT_FULL                  |     y      |   kspp   | self_protection  | FAIL: is not found
CONFIG_RANDSTRUCT_PERFORMANCE           | is not set |   kspp   | self_protection  | FAIL: CONFIG_RANDSTRUCT_FULL is not "y"
CONFIG_GCC_PLUGIN_LATENT_ENTROPY        |     y      |   kspp   | self_protection  | FAIL: CONFIG_GCC_PLUGINS is not "y"
CONFIG_UBSAN_BOUNDS                     |     y      |   kspp   | self_protection  | FAIL: is not found
CONFIG_UBSAN_LOCAL_BOUNDS               |     y      |   kspp   | self_protection  | FAIL: is not found
CONFIG_UBSAN_TRAP                       |     y      |   kspp   | self_protection  | FAIL: CONFIG_UBSAN_BOUNDS is not "y"
CONFIG_UBSAN_SANITIZE_ALL               |     y      |   kspp   | self_protection  | FAIL: CONFIG_UBSAN_BOUNDS is not "y"
CONFIG_GCC_PLUGIN_STACKLEAK             |     y      |   kspp   | self_protection  | FAIL: CONFIG_GCC_PLUGINS is not "y"
CONFIG_STACKLEAK_METRICS                | is not set |   kspp   | self_protection  | FAIL: CONFIG_GCC_PLUGINS is not "y"
CONFIG_STACKLEAK_RUNTIME_DISABLE        | is not set |   kspp   | self_protection  | FAIL: CONFIG_GCC_PLUGINS is not "y"
CONFIG_CFI_CLANG                        |     y      |   kspp   | self_protection  | FAIL: is not found
CONFIG_CFI_PERMISSIVE                   | is not set |   kspp   | self_protection  | FAIL: CONFIG_CFI_CLANG is not "y"
CONFIG_SECURITY_SELINUX_BOOTPARAM       | is not set |   kspp   | security_policy  | FAIL: "y"
CONFIG_SECURITY_SELINUX_DEVELOP         | is not set |   kspp   | security_policy  | FAIL: "y"
CONFIG_MODULES                          | is not set |   kspp   |cut_attack_surface| FAIL: "y"
CONFIG_FAIL_FUTEX                       | is not set |  grsec   |cut_attack_surface| OK: is not found
CONFIG_KCMP                             | is not set |  grsec   |cut_attack_surface| FAIL: "y"
CONFIG_FB                               | is not set |maintainer|cut_attack_surface| FAIL: "y"
CONFIG_VT                               | is not set |maintainer|cut_attack_surface| FAIL: "y"
CONFIG_USER_NS                          | is not set |  clipos  |cut_attack_surface| FAIL: "y"
CONFIG_BPF_SYSCALL                      | is not set | lockdown |cut_attack_surface| FAIL: "y"

```
[+] Config check is finished: 'OK' - 169 / 'FAIL' - 27

