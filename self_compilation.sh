#!/bin/bash
## Script is designed to streamline the self-compilation process for the end-user. 
## This entails staging linux-hardened, pulling PlagueOS kernel configuration, fingerprinting hardware, then compiling the minimalist kernel. 
## Main benefit of self-compilation is you are not reliant on the upstream hardened configuration that must support various classes of hardware.
### This kernel is your own. 

# Ensure /boot is writeable
mount -o remount,rw /boot

# Set Kernel Version (KVER)
function set_kver() {
    echo "Enter the kernel version to use (e.g., '6.6.18-hardened1'):"
    while true; do
        read -e -i "${KVER:-}" -p "" KVER
        if [[ $(echo $KVER | grep -E '^[0-9]+\.[0-9]+\.[0-9]+(-hardened1)$') == '' ]]; then
            echo "Invalid format.";
        else
            break;
        fi
    done
}

# Invoke function to prompt end-user for desired version (must be an active release in the Anthraxx Linux-Hardened repository)
set_kver

function check_distro(){
    declare -r distroId="$(awk -F= '$1=="ID"{print $NF}' /etc/os-release)"
    case "${distroId,,}" in
        *void*)
            printf '%s\n' "Detected Void Linux..."
            xbps-install -Sy make gcc xz elfutils elfutils-devel flex ncurses-devel openssl openssl-devel argp-standalone gcc-ada mpc libmpc-devel gmp-devel perl zstd
            return 0
          ;;
        *debian*|*ubuntu*)
            printf '%s\n' "Detected Debian-based Distribution..."
            sudo apt-get install build-essential linux-source bc kmod cpio flex libncurses5-dev libelf-dev libssl-dev dwarves bison zstd
            return 0
          ;;
        *fedora*|*redhat*)
            printf '%s\n' "Detected RHEL-based Distribution..."
            sudo dnf install binutils elfutils-libelf-devel openssl-devel bc bison flex gcc git openssl make perl dwarves zstd 
            return 0
          ;;
         *)
             printf '%s\n' "Unable to detect Operating System!" >&2
             return 1
           ;;
    esac
}

check_distro

# Staging w/ error handling
# Pull down Anthraxx linux-hardened upstream
if [[ ! -f /usr/src/"$KVER".tar.gz ]]; then
        /usr/bin/curl --verbose --tlsv1.3 --proto =https -L -O --url "https://github.com/anthraxx/linux-hardened/archive/refs/tags/v$KVER.tar.gz"
fi

if [[ ! -d /usr/src/linux-hardened-"$KVER" ]]; then
        tar -xf v"$KVER".tar.gz -C /usr/src/
fi

# Move to staging directory
cd /usr/src/linux-hardened-"$KVER"

# Pull down plague kconfig
read -rp 'Are you compiling this kernel for a physical machine (host) or virtual machine (VM)? [host/vm]: ' response
if [[ "${response,,}" = "vm" ]]
then
   echo "Compiling a VM kernel"
   wget https://0xacab.org/optout/plague-kernel/-/raw/main/virt_hardened.config -O .config
   ## virt_hardened.config is still a WIP
elif [[ "${response,,}" = "host" ]]
then
   echo "Compiling a host kernel"
   wget https://0xacab.org/optout/plague-kernel/-/raw/main/host_hardened.config -O .config
else
   echo "Invalid input. Please choose either \"Host\" or \"VM\"."
   exit 1
fi

# Prompt if baseline Plague kernel is desired
## If not, proceed to fingerprint device
read -p "Do you want create a custom kernel tailored to your hardware? (y/n)" response
response=$(echo $response | tr '[:upper:]' '[:lower:]')
if [[ $response == "y" ]]; then
    echo "Tailoring kernel configuration to your hardware"
    make localmodconfig
else
    echo "Using baseline Plague kernel configuration"
fi

read -p "Do you want to open the kernel configuration editor? (y/n)" response
response=$(echo $response | tr '[:upper:]' '[:lower:]')
if [[ $response == "y" ]]; then
    make menuconfig
else
    echo "Proceeding to compile"
fi

# compile
make -j $(nproc --all)
make modules_install INSTALL_MOD_STRIP=1 install

case "${distroId,,}" in
        *void*)
            cp ./arch/x86_64/boot/bzImage /boot/vmlinuz-"$KVER"
                dracut --kver "$KVER" --force
                grub-mkconfig -o /boot/grub/grub.cfg
                xbps-reconfigure -fa
                /usr/bin/update-grub            
            return 0
          ;;
        *debian*|*ubuntu*)
            cp ./arch/x86_64/boot/bzImage /boot/vmlinuz-"$KVER"
                dracut --kver "$KVER" --force
            update-grub2
            return 0
          ;;
        *fedora*|*redhat*)
            command -v installkernel
            return 0
          ;;
         *)
             printf '%s\n' "Unable to detect Operating System!" >&2
             return 1
           ;;
    esac


# Remove sysmap/signing keys
rm /lib/modules/"$KVER"/source/certs/signing_key*
rm /lib/modules/"$KVER"/source/System.map
rm /lib/modules/"$KVER"/source
rm /lib/modules/"$KVER"/build

echo "Congrats! Your custom kernel based on the PlagueOS kernel configuration has been installed."
echo "Reboot now? (y/N): "
read reboot_opt

if [[ "$reboot_opt" == "Y" ]]; then
        reboot
else
        exit
fi
